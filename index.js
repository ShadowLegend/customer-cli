
const mongoose = require ( "mongoose" );
const Customer = require ( "./models/customer" );
const chalk = require ( "chalk" );

mongoose.Promise = global.Promise;

const database = mongoose.connect ( "mongodb://localhost:27017/customerCli" );

const addCustomer = function ( customer ) {

    Customer
        .create ( customer )
        .then ( function ( customer ) {

            console.info ( chalk.green ( "New Customer Added" ) );
            mongoose.disconnect ();

        })
        .catch ( function ( error ) {

            if ( error ) {

                console.error ( chalk.red ( error ) );
                mongoose.disconnect ();

            }

        });

};

const findCustomer = function ( name ) {

    const search = new RegExp ( name, 'i' );

    Customer
        .find ({

            $or: [{

                firstname: search

            }, {

                lastname: search

            }]

        })
        .then ( function ( customer ) {

            console.info ( customer );
            console.info ( chalk.green ( `${ customer.length } matches` ) );
            mongoose.disconnect ();

        })
        .catch ( function ( error ) {

            if ( error ) {

                console.error ( chalk.red ( error ) );
                mongoose.disconnect ();

            }

        });

};

const updateCustomer = function ( _id, customer ) {

    if ( _id ) {

        Customer
            .update ( { _id }, customer )
            .then ( function ( customer ) {

                console.info ( chalk.green ( "Customer has been updated" ) );
                mongoose.disconnect ();

            })
            .catch ( function ( error ) {

                if ( error ) {

                    console.error ( chalk.red ( error ) );
                    mongoose.disconnect ();

                }

            });

    }

};

const removeCustomer = function ( _id ) {

    if ( _id ) {

        Customer
            .remove ( { _id } )
            .then ( function ( customer ) {

                console.info ( chalk.green ( "Customer has been removed" ) );
                mongoose.disconnect ();

            })
            .catch ( function ( error ) {

                if ( error ) {

                    console.error ( chalk.red ( error ) );
                    mongoose.disconnect ();

                }

            });

    }

};

const listCustomer = function () {

    Customer
        .find ()
        .then ( function ( customer ) {

            console.info ( customer );
            console.info ( chalk.green ( `${ customer.length } matches` ) );
            mongoose.disconnect ();

        })
        .catch ( function ( error ) {

            if ( error ) {

                console.error ( chalk.red ( error ) );
                mongoose.disconnect ();

            }

        });

};

module.exports = { addCustomer, findCustomer, updateCustomer, removeCustomer, listCustomer };