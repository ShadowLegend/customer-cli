#!/usr/bin/env node

const program = require ( "commander" );
const { prompt } = require ( "inquirer" );

// customer questions
const questions = [{

    type: 'input',
    name: 'firstname',
    message: 'Customer firstname'

}, {
    
    type: 'input',
    name: 'lastname',
    message: 'Customer lastname'
    
}, {

    type: 'input',
    name: 'phone',
    message: 'Customer phone number'

}, {

    type: 'input',
    name: 'email',
    message: 'Customer email address'

}];

const {

    addCustomer,
    findCustomer,
    updateCustomer,
    removeCustomer,
    listCustomer

} = require ( "./index" );

program
    .version ( "1.0.0" )
    .description ( "Client Mangement System" );

program
    .command ( "add" )
    .alias ( "a" )
    .description ( "Add a customer" )
    .action ( function () {

        prompt ( questions ).then ( function ( inputs ) {

            addCustomer ( inputs );

        });

    });

program
    .command ( "find <name>" )
    .alias ( "f" )
    .description ( "Find a customer" )
    .action ( function ( name ) {

        findCustomer ( name );

    });

program
    .command ( "update <_id>" )
    .alias ( "u" )
    .description ( "Update a customer" )
    .action ( function ( _id ) {

        prompt ( questions ).then ( function ( inputs ) {

            updateCustomer ( _id, inputs );

        });

    });

program
    .command ( "remove <_id>" )
    .alias ( "r" )
    .description ( "Remove a customer" )
    .action ( function ( _id ) {

        removeCustomer ( _id );

    });

program
    .command ( "list" )
    .alias ( "l" )
    .description ( "List customers" )
    .action ( function () {

        listCustomer ();

    });

program.parse ( process.argv );